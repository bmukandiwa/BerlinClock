//
// Created by bmukandiwa on 28/1/2024.
//

import Foundation
extension BerlinClockViewModel {

    func extractSeconds(from time: String) -> Int? {
        let components = time.components(separatedBy: ":")
        guard components.count == 3, let seconds = Int(components[2]) else {
            return nil
        }
        return seconds
    }

    func extractMinutes(from time: String) throws -> Int {
            let components = time.components(separatedBy: ":")
            guard components.count == 3 else {
                throw ExtractError.invalidFormat
            }
            guard let minutes = Int(components[1]) else {
                throw ExtractError.invalidFormat
            }

            return minutes
        }

    func extractFiveMinutes(from time: String)  -> Int {
        let components = time.components(separatedBy: ":")
        guard components.count == 3, let minutes = Int(components[1]) else {
            return 0
        }
        return minutes
    }

    func extractHours(from time: String) throws -> Int {
        let components = time.components(separatedBy: ":")
        guard components.count == 3, let hours = Int(components[0]),(0..<24).contains(hours) else {
            throw ExtractError.invalidFormat
        }
        return hours
    }

    func extractFiveHours(from time: String) throws -> Int {
        let components = time.components(separatedBy: ":")
        guard components.count == 3 else {
            throw ExtractError.invalidFormat
        }

        guard let hours = Int(components[0]), (0..<24).contains(hours) else {
            throw ExtractError.invalidHour
        }

        return hours
    }



    }
