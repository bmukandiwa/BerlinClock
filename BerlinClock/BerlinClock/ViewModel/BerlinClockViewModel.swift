//
// Created by bmukandiwa on 28/1/2024.
//

import Foundation

class BerlinClockViewModel {

    func getSecondsLamp(for time: String) -> String {
        guard let seconds = extractSeconds(from: time) else {
            return "The Second Row for did not match the expected value."
        }
        return seconds % 2 == 0 ? "Y" : "O"
    }

    func getSingleMinutesRow(for time: String) -> String {
        do {
            let minutes = try extractMinutes(from: time)
            let singleMinutesRow = String(repeating: "Y", count: minutes % 5) + String(repeating: "O", count: 4 - (minutes % 5))
            return singleMinutesRow
        } catch {
            return "The Single Minute Row for did not match the expected value."
        }

    }

    func getFiveMinutesRow(for time: String) -> String {
        let minutes = extractFiveMinutes(from: time)
        var fiveMinutesRow = ""

        for i in 1...11 {
            if i <= minutes / 5 {
                fiveMinutesRow += (i % 3 == 0) ? "R" : "Y"
            } else {
                fiveMinutesRow += "O"
            }
        }

        return fiveMinutesRow



    }

    func getSingleHoursRow(for time: String) -> String {
        do{
                let hours = try extractHours(from: time)
                return String(repeating: "R", count: hours % 5) + String(repeating: "O", count: 4 - (hours % 5))

            } catch {
                return "The Single Hour Row for did not match the expected value."

        }
    }

    func getFiveHoursRow(for time: String) -> String {
        do {
            let hours = try extractFiveHours(from: time)
            return String(repeating: "R", count: hours / 5) + String(repeating: "O", count: 4 - (hours / 5))
        } catch {
            return "The Five Hour Row for did not match the expected value."
        }

    }

}