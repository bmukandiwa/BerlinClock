//
// Created by bmukandiwa on 28/1/2024.
//

import Foundation

class BerlinClockMainViewModel {
    private let secondsViewModel: BerlinClockViewModel
    private let singleHoursViewModel: BerlinClockViewModel
    private let fiveHoursViewModel: BerlinClockViewModel
    private let singleMinutesViewModel: BerlinClockViewModel
    private let fiveMinutesViewModel: BerlinClockViewModel

    init(secondsViewModel: BerlinClockViewModel = BerlinClockViewModel(),
         singleHoursViewModel: BerlinClockViewModel = BerlinClockViewModel(),
         fiveHoursViewModel: BerlinClockViewModel = BerlinClockViewModel(),
         singleMinutesViewModel: BerlinClockViewModel = BerlinClockViewModel(),
         fiveMinutesViewModel: BerlinClockViewModel = BerlinClockViewModel()) {

        self.secondsViewModel = secondsViewModel
        self.singleHoursViewModel = singleHoursViewModel
        self.fiveHoursViewModel = fiveHoursViewModel
        self.singleMinutesViewModel = singleMinutesViewModel
        self.fiveMinutesViewModel = fiveMinutesViewModel
    }

    func getBerlinClock(for time: String) -> String {
        let secondsLamp = secondsViewModel.getSecondsLamp(for: time)
        let singleHoursRow = singleHoursViewModel.getSingleHoursRow(for: time)
        let fiveHoursRow = fiveHoursViewModel.getFiveHoursRow(for: time)
        let singleMinutesRow = singleMinutesViewModel.getSingleMinutesRow(for: time)
        let fiveMinutesRow = fiveMinutesViewModel.getFiveMinutesRow(for: time)

        let berlinClock = "\(secondsLamp)\(fiveHoursRow)\(singleHoursRow)\(fiveMinutesRow)\(singleMinutesRow)"
        return berlinClock
    }

    func getSecondsLamp(for time: String) -> String {
         secondsViewModel.getSecondsLamp(for: time)
    }

    func getFiveHoursRow(for time: String) -> String {
         fiveHoursViewModel.getFiveHoursRow(for: time)
    }

    func getSingleHoursRow(for time: String) -> String {
         singleHoursViewModel.getSingleHoursRow(for: time)
    }

    func getFiveMinutesRow(for time: String) -> String {
         fiveMinutesViewModel.getFiveMinutesRow(for: time)
    }

    func getSingleMinutesRow(for time: String) -> String {
         singleMinutesViewModel.getSingleMinutesRow(for: time)
    }

}