//
//  BerlinClockView.swift
//  BerlinClock
//
//  Created by bmukandiwa on 28/1/2024.
//

import Foundation
import SwiftUI

struct BerlinClockView: View {
    @State private var time: String = "15:29:40"
    @State private var heading: String = "Berlin Clock"

    
    let viewModel = BerlinClockViewModel()
    var timer: Timer?
    
    
   private struct Accessibility {
         static let singleMinuteRow = "singleMinuteRow"
         static let minutesRow = "minutesRow"
         static let hoursRow = "hoursRow"
         static let fiveHourRow = "fiveHourRow"
         static let redLampView = "redLampView"
         static let berlinClockTitle = "berlinClockTitle"
         static let timeLabel = "timeLabel"
       
}
    
    
var body: some View {
        VStack {
            Text(time)
                .font(.system(size: 36, weight: .bold, design: .default))
                .padding()

                .accessibilityIdentifier(Accessibility.timeLabel)


            VStack(spacing: 10) {

                Text(heading)
                    .font(.title)
                    .accessibilityIdentifier(Accessibility.redLampView)

                LampView(color: .red, isOn: viewModel.getSecondsLamp(for: time) == "Y")
                    .frame(width: 30, height: 30)

                .accessibilityIdentifier(Accessibility.redLampView)


                FiveHourRowView(color: .red, count: 4, activeCount: countActiveLamps(viewModel.getFiveHoursRow(for: time)))

                    .accessibilityIdentifier(Accessibility.fiveHourRow)


                HoursRowView(color: .red, count: 4, activeCount: countActiveLamps(viewModel.getSingleHoursRow(for: time)))

                    .accessibilityIdentifier(Accessibility.hoursRow)


                MinutesRowView(color: .yellow, count: 11, activeCount: countActiveLamps(viewModel.getFiveMinutesRow(for: time)))

                    .accessibilityIdentifier(Accessibility.minutesRow)


                SingleMinuteRowView(color: .yellow, count: 4, activeCount: countActiveLamps(viewModel.getSingleMinutesRow(for: time)))

                    .accessibilityIdentifier(Accessibility.singleMinuteRow)

            }
            .padding()
            .background(Color.gray)

        }
        .onAppear {
            startTimer()
        }
        .onDisappear {
            stopTimer()
        }
    }

    private func startTimer() {
        _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            updateTime()
        }
    }

    private func stopTimer() {
        timer?.invalidate()
    }

    private func updateTime() {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let currentTime = formatter.string(from: Date())
        time = currentTime
    }

    private func countActiveLamps(_ row: String) -> Int {
        return row.filter { $0 == "Y" }.count
    }
}

struct BerlinClockController_Previews: PreviewProvider {
    static var previews: some View {
        BerlinClockView()
    }
}
