//
//  BerlinClockApp.swift
//  BerlinClock
//
//  Created by bmukandiwa on 28/1/2024.
//

import SwiftUI


@main
struct BerlinClockApp: App {
    var body: some Scene {
        WindowGroup {
            BerlinClockView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        BerlinClockView()
    }
}
