//
//  BerlinClockMainView.swift
//  BerlinClock
//
//  Created by bmukandiwa on 28/1/2024.
//

import Foundation
import SwiftUI


struct BerlinClockMainView: View {
    var viewModel: BerlinClockViewModel
    var time: String

    var body: some View {
        VStack(spacing: 10) {
            Text("Berlin Clock")
                    .font(.title)
                    .accessibilityIdentifier("berlinClockTitle")
            

            // Top Lamp (Seconds)
            LampView(color: .red, isOn: viewModel.getSecondsLamp(for: time) == "Y")
                    .frame(width: 30, height: 30)
                    .accessibilityIdentifier("redLampView")


                FiveHourRowView(color: .red, count: 4, activeCount: countActiveLamps(viewModel.getFiveHoursRow(for: time)))
                .accessibilityIdentifier("fiveHourRow")


                HoursRowView(color: .red, count: 4, activeCount: countActiveLamps(viewModel.getSingleHoursRow(for: time)))
                .accessibilityIdentifier("hoursRow")



               MinutesRowView(color: .yellow, count: 11, activeCount: countActiveLamps(viewModel.getFiveMinutesRow(for: time)))
                .accessibilityIdentifier("minutesRow")


               SingleMinuteRowView(color: .yellow, count: 4, activeCount: countActiveLamps(viewModel.getSingleMinutesRow(for: time)))
                .accessibilityIdentifier("singleMinuteRow")

        }
                .padding()
                .background(Color.gray)
                .accessibilityIdentifier("berlinClockMainView")

    }


    private func countActiveLamps(_ row: String) -> Int {
        return row.filter { $0 == "Y" }.count
    }
}
