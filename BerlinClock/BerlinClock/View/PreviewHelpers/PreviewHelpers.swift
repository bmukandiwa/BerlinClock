//
//  PreviewHelpers.swift
//  BerlinClock
//
//  Created by bmukandiwa on 28/1/2024.
//

import Foundation
import SwiftUI
struct LampView: View {
    var color: Color
    var isOn: Bool

    var body: some View {
        Circle()
                .fill(isOn ? color : Color.white)
                .frame(width: 40, height: 40)
                .accessibility(identifier: "lampView")

    }
}

struct FiveHourRowView: View {
    var color: Color
    var count: Int
    var activeCount: Int

    var body: some View {
        HStack(spacing: 5) {
            ForEach(0..<count, id: \.self) { index in
                HorizontalLampView(color: color, isOn: index < activeCount)
            }
        }
                .accessibility(identifier: "fiveHourRowView")
    }
}

struct HoursRowView: View {
    var color: Color
    var count: Int
    var activeCount: Int

    var body: some View {
        HStack(spacing: 5) {
            ForEach(0..<count, id: \.self) { index in
                HorizontalLampView(color: color, isOn: index < activeCount)
            }
        }
                .accessibility(identifier: "hoursRowView")

    }
}

struct MinutesRowView: View {
    var color: Color
    var count: Int
    var activeCount: Int

    var body: some View {
        HStack(spacing: 5) {
            ForEach(0..<count, id: \.self) { index in
                VerticalLampView(color: index % 3 == 2 ? .red : color, isOn: index < activeCount)
            }
        }
    }
}

struct SingleMinuteRowView: View {
    var color: Color
    var count: Int
    var activeCount: Int

    var body: some View {
        HStack(spacing: 5) {
            ForEach(0..<count, id: \.self) { index in
                HorizontalLampView(color: .yellow, isOn: index < activeCount)
            }
        }
                .accessibility(identifier: "singleMinuteRowView")

    }
}
struct HorizontalLampView: View {
    var color: Color
    var isOn: Bool

    var body: some View {
        Rectangle()
                .fill(isOn ? color : Color.white)
                .frame(width: 70, height: 50)
                .accessibility(identifier: "horizontalLampView")

    }
}

struct VerticalLampView: View {
    var color: Color
    var isOn: Bool

    var body: some View {
        Rectangle()
                .fill(isOn ? color : Color.white)
                .frame(width: 30, height: 50)
                .accessibility(identifier: "verticalLampView")

    }
}
