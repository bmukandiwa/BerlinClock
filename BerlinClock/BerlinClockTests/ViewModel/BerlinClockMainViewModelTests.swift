//
// Created by bmukandiwa on 28/1/2024.
//

import Foundation
import XCTest
@testable import BerlinClock

class BerlinClockMainViewModelTests: XCTestCase {
    let invalidResult = "INVALID"
    var mainViewModel: BerlinClockMainViewModel!

    override func setUp() {
        super.setUp()
        mainViewModel = BerlinClockMainViewModel()
    }

    override func tearDown() {
        mainViewModel = nil
        super.tearDown()
    }

    func testGetBerlinClock() {
        let midnightBerlinClock = mainViewModel.getBerlinClock(for: "00:00:00")
        XCTAssertEqual(midnightBerlinClock, "YOOOOOOOOOOOOOOOOOOOOOOO")


        let sixteenFiftyBerlinClock = mainViewModel.getBerlinClock(for: "16:50:06")
        XCTAssertEqual(sixteenFiftyBerlinClock, "YRRROROOOYYRYYRYYRYOOOOO")


        let endOfDayBerlinClock = mainViewModel.getBerlinClock(for: "23:59:59")
        XCTAssertEqual(endOfDayBerlinClock, "ORRRRRRROYYRYYRYYRYYYYYY")


        let elevenThirtySevenBerlinClock = mainViewModel.getBerlinClock(for: "11:37:01")
        XCTAssertEqual(elevenThirtySevenBerlinClock, "ORROOROOOYYRYYRYOOOOYYOO")

    }
}