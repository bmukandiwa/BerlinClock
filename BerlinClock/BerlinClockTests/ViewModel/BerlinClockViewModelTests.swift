//
// Created by bmukandiwa on 28/1/2024.
//

import XCTest
@testable import BerlinClock

final class BerlinClockViewModelTests: XCTestCase {

    func testSecondsLamp() {
        let viewModel = BerlinClockViewModel()

        var lamp = viewModel.getSecondsLamp(for: "00:00:00")
        XCTAssertEqual(lamp, "Y")


        lamp = viewModel.getSecondsLamp(for: "23:59:59")
        XCTAssertEqual(lamp, "O")


        lamp = viewModel.getSecondsLamp(for: "invalidTimeFormat")
        XCTAssertEqual(lamp, "The Second Row for did not match the expected value.")


        lamp = viewModel.getSecondsLamp(for: "12:34:abc")
        XCTAssertEqual(lamp, "The Second Row for did not match the expected value.")


        lamp = viewModel.getSecondsLamp(for: "12:34")
        XCTAssertEqual(lamp, "The Second Row for did not match the expected value.")

    }

    func testSingleMinutes() {
        let viewModel = BerlinClockViewModel()

        var row = viewModel.getSingleMinutesRow(for: "00:00:00")
        XCTAssertEqual(row, "OOOO")


        row = viewModel.getSingleMinutesRow(for: "12:34:00")
        XCTAssertEqual(row, "YYYY")


        row = viewModel.getSingleMinutesRow(for: "12:32:00")
        XCTAssertEqual(row, "YYOO")


        row = viewModel.getSingleMinutesRow(for: "12:35:00")
        XCTAssertEqual(row, "OOOO")


        row = viewModel.getSingleMinutesRow(for: "23:59:59")
        XCTAssertEqual(row, "YYYY")

        row = viewModel.getSingleMinutesRow(for: "invalidTimeFormat")
        XCTAssertEqual(row, "The Single Minute Row for did not match the expected value.")
    }


    func testFiveMinuteRow(){
        let viewModel = BerlinClockViewModel()

        var row = viewModel.getFiveMinutesRow(for: "00:00:00")
        XCTAssertEqual(row, "OOOOOOOOOOO")


        row = viewModel.getFiveMinutesRow(for: "12:04:00")
        XCTAssertEqual(row, "OOOOOOOOOOO")


        row = viewModel.getFiveMinutesRow(for: "12:23:00")
        XCTAssertEqual(row, "YYRYOOOOOOO")


        row = viewModel.getFiveMinutesRow(for: "12:35:00")
        XCTAssertEqual(row, "YYRYYRYOOOO")


        row = viewModel.getFiveMinutesRow(for: "23:59:59")
        XCTAssertEqual(row, "YYRYYRYYRYY")
    }


    func testOneHourRow(){
        let viewModel = BerlinClockViewModel()


        var row = viewModel.getSingleHoursRow(for: "00:00:00")
        XCTAssertEqual(row, "OOOO")


        row = viewModel.getSingleHoursRow(for: "08:23:00")
        XCTAssertEqual(row, "RRRO")


        row = viewModel.getSingleHoursRow(for: "14:35:00")
        XCTAssertEqual(row, "RRRR")


        row = viewModel.getSingleHoursRow(for: "23:59:59")
        XCTAssertEqual(row, "RRRO")


        row = viewModel.getSingleHoursRow(for: "02:04:00")
        XCTAssertEqual(row, "RROO")


        row = viewModel.getSingleHoursRow(for: "invalidTimeFormat")
        XCTAssertEqual(row, "The Single Hour Row for did not match the expected value.")


        row = viewModel.getSingleHoursRow(for: "25:30:00")
        XCTAssertEqual(row, "The Single Hour Row for did not match the expected value.")


        row = viewModel.getSingleHoursRow(for: "08:23")
        XCTAssertEqual(row, "The Single Hour Row for did not match the expected value.")


    }


    func testFiveHoursRow() {
        let viewModel = BerlinClockViewModel()
        var row = viewModel.getFiveHoursRow(for: "00:00:00")
        XCTAssertEqual(row, "OOOO")


        row = viewModel.getFiveHoursRow(for: "08:23:00")
        XCTAssertEqual(row, "ROOO")


        row = viewModel.getFiveHoursRow(for: "23:59:59")
        XCTAssertEqual(row, "RRRR")


        row = viewModel.getFiveHoursRow(for: "02:04:00")
        XCTAssertEqual(row, "OOOO")


        row = viewModel.getFiveHoursRow(for: "16:35:00")
        XCTAssertEqual(row, "RRRO")


        row = viewModel.getFiveHoursRow(for: "invalidTimeFormat")
        XCTAssertEqual(row, "The Five Hour Row for did not match the expected value.")


        row = viewModel.getFiveHoursRow(for: "25:30:00")
        XCTAssertEqual(row, "The Five Hour Row for did not match the expected value.")


        row = viewModel.getFiveHoursRow(for: "08:23")
        XCTAssertEqual(row, "The Five Hour Row for did not match the expected value.")
    }

    }
