//
// Created by bmukandiwa on 28/1/2024.
//

import XCTest

class BerlineClockUITests: XCTestCase {

    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    override func tearDown() {
        super.tearDown()
        // Add any cleanup code if needed
    }

    func testBerlinClockView() {
        // Access elements and perform assertions for BerlinClockView
        XCTAssertTrue(app.staticTexts["timeLabel"].exists)
        XCTAssertTrue(app.otherElements["berlinClockMainView"].exists)

        // You can add more assertions based on your actual implementation
    }

    func testBerlinClockMainView() {
        // Access elements and perform assertions for BerlinClockMainView
        XCTAssertTrue(app.staticTexts["berlinClockTitle"].exists)
        XCTAssertTrue(app.otherElements["redLampView"].exists)
        XCTAssertTrue(app.otherElements["fiveHourRow"].exists)
        XCTAssertTrue(app.otherElements["hoursRow"].exists)
        XCTAssertTrue(app.otherElements["minutesRow"].exists)
        XCTAssertTrue(app.otherElements["singleMinuteRow"].exists)

        // You can add more assertions based on your actual implementation
    }
}
