//
//  BerlinClockUITests.swift
//  BerlinClockUITests
//
//  Created by bmukandiwa on 28/1/2024.
//



import XCTest

final class BerlinClockUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    func testBerlinClockViewIsRunning() throws {
        let app = XCUIApplication()
        app.launch()
        
        XCTAssertTrue(app.staticTexts["timeLabel"].exists)
        XCTAssertTrue(app.staticTexts["redLampView"].exists)
        XCTAssertTrue(app.otherElements["singleMinuteRow"].exists)
        XCTAssertTrue(app.otherElements["minutesRow"].exists)
        XCTAssertTrue(app.otherElements["hoursRow"].exists)
        XCTAssertTrue(app.otherElements["fiveHourRow"].exists)
      
       
    }

}

